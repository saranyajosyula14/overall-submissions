class Node:
   def __init__(self, dataval=None):
      self.dataval = dataval
      self.nextval = None
class SLinkedList:
   def __init__(self):
      self.headval = None

# Function to add node
   def Index(self,middle_node,newdata):
      if middle_node is None:
         print("The mentioned node is absent")
         return

      NewNode = Node(newdata)
      NewNode.nextval = middle_node.nextval
      middle_node.nextval = NewNode

# Print the linked list
   def listprint(self):
      printval = self.headval
      while printval is not None:
         print (printval.dataval,end='->')
         printval = printval.nextval

list = SLinkedList()
list.headval = Node("saranya")
e2 = Node("sudheer")
e3 = Node("charan")

list.headval.nextval = e2
e2.nextval = e3

list.Index(list.headval.nextval,"teja")

list.listprint()