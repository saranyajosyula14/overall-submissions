def palindrome(data):
    if type(data) != str and type(data) != list:
        return False
    if type(data)==str:
        data=data.lower()
    rev=data[::-1]
    if data == rev:
        return True
    return False

if __name__=='__main__':
    print(palindrome("abcde"))
    print(palindrome([2,2,3,2,2]))
    print(palindrome("saranya"))
