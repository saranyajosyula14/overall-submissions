'''importing random and string modules'''
import random, string


number_of_digits = int(input("number of digits you wnt in your password:"))
number_of_punctuation_characters = int(input(""))
characters = string.ascii_letters + string.digits + string.punctuation
'''creating a class called password_gen to create methods'''
class password_gen:
  '''
	class to generate random password and check strongness of the password
	...

	Methods
	-------
	password_gen(self)
		generates the random password with given parameters

	pass_strength(self,*args)
		checks the strength of the given password
  
	'''
  #creating a method called gen_password 
  def gen_password(self):
    # getting inputs from user 

    number_of_passwords = int(input("How many passwords do you want to generate? "))

    password_length = int(input("Provide the password length: "))
    #looping in number of passwords
    for password_index in range(number_of_passwords):
      password = ""
      #looping through number_of_digits
      for digits_index in range(number_of_digits):
        password=password +random.choice(string.digits)
        #looping through number_of_punctuation_characters
        for punctuation_index in range(number_of_punctuation_characters):
          password=password+random.choice(string.punctuation)
          #looping through the final password requirements
          for index in range(password_length-number_of_digits-number_of_punctuation_characters):
            password=password+random.choice(string.ascii_letters)
          #printing the password 
          print("password {} generated: {} ".format(password_index,password))
  '''method 
  to create a method to print the strength of the password'''
  def strnthofpassword(self,*args):

    for x in args:
      n = len(x)

      # Checking lower alphabet in string
      Lower = False
      # Checking upper alphabet in string
      Upper = False
      # Checking digits in the password
      Digit = False
      #checking specail characters in the password
      specialChar = False
      #Checking normal characters in the password
      normalChars = "abcdefghijklmnopqrstu"
      "vwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 "
      '''
      checking whether the given password is in the given lower alphabets,upper alphabets,digits,special characters,normal chracters'''
      for i in range(n):
        if x[i].islower():
          Lower = True
        if x[i].isupper():
          Upper = True
        if x[i].isdigit():
          Digit = True
        if x[i] not in normalChars:
          specialChar = True
  
      # Strength of password
      print("Strength of password for "+x+"is : ", end = "")
      if (Lower and Upper and
        Digit and specialChar and n >= 8):
        print("Strong")
          
      elif ((Lower or Upper) and
        specialChar and n >= 8):
        print("Moderate")
      else:
        print("Weak")





if __name__=="__main__":
  res=password_gen()
  res.gen_password()
  res.strnthofpassword('9,<4@[8"[')